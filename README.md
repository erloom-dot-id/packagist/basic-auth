## Requirement

- Add this to your root of `composer.json` :
```
"repositories": [
    {
        "type": "gitlab",
        "url":  "git@gitlab.com:erloom-dot-id/packagist/basic-auth.git"
    }
]
```

- Add the package to the `require` section of `composer.json` :
```
"require": {
    ...
    "erloom/basicauth": "dev-master"
}
```

- Run `composer update` or `composer install` for your first installation project

- Run this command :
`php artisan vendor:publish --provider="Erloom\Basicauth\AuthServiceProvider"`

- Add these to your env (this will be used for authentication between 2 or more applications) :
```
EAUTH_USERNAME=
EAUTH_PASSWORD=
```

- See Examples for further implementation


## Facades

There will be 2 facades available :

- **makeCredential()** - will return token used for auth
`Eauth::makeCredential()`

- **validateCredential($token)** - will validate token retrieved
`Eauth::validateCredential($token)`