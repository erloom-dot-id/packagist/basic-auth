<?php

namespace Erloom\Basicauth\Facades;

use Illuminate\Support\Facades\Facade;

class Eauth extends Facade {
    public static function getFacadeAccessor() {
        return 'eauth';
    }
}