<?php

namespace Erloom\Basicauth;

use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider {

    public function boot() {
        $this->publishes([
            __DIR__ . '/Config/eauth.php' => config_path('eauth.php'),
        ]);
    }
    
    public function register() {

        $this->app->singleton('eauth', function ($app) {
            return new Eauth();
        });

        $this->app->alias('eauth', Eauth::class);
    }

}

?>