<?php

namespace Erloom\Basicauth;

use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;

class Eauth {

    public function validateCredential($token) {

        try {
            $auth = [
                'username' => config('eauth.auth.username'),
                'password' => config('eauth.auth.password')
            ];
            $serialize = serialize($auth);

            if (!Hash::check($serialize, $token)) {
                throw new Exception();
            }

            return true;

        } catch (Exception $e) {
            return false;
        }

    }

    public function makeCredential() {

        try {

            $auth = [
                'username' => config('eauth.auth.username'),
                'password' => config('eauth.auth.password')
            ];
            $serialize = serialize($auth);

            $token = Hash::make($serialize, [
                'rounds' => 12
            ]);

            return $token;

        } catch (Exception $e) {
            return false;
        }

    }
}