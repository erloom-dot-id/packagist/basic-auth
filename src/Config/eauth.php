<?php

return [
    'auth' => [
        'username' => env('EAUTH_USERNAME'),
        'password' => env('EAUTH_PASSWORD'),
    ],
];