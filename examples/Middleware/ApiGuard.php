<?php

namespace App\Http\Middleware;

use Closure;
use Erloom\Basicauth\Facades\Eauth;

class ApiGuard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $validate = Eauth::validateCredential($request->bearerToken());
        if(!$validate) {
            return response('Request Invalid', 401);
        }

        return $next($request);
    }
}