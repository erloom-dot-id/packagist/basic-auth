<?php

use Erloom\Basicauth\Facades\Eauth;

Route::get('/token', function () {
    return Eauth::makeCredential();
});